/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "MM_Digitization/MM_DigitizationTool.h"

DECLARE_COMPONENT(MM_DigitizationTool)
