#include "../IntegrationBase.h"
#include "../PixelClustering.h"
#include "../Spacepoints.h"
#include "../DataPreparationPipeline.h"
#include "../ClusterContainerMaker.h"

DECLARE_COMPONENT(IntegrationBase)
DECLARE_COMPONENT(PixelClustering)
DECLARE_COMPONENT(Spacepoints)
DECLARE_COMPONENT(DataPreparationPipeline)
DECLARE_COMPONENT(ClusterContainerMaker)